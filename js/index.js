"use strict";

var screenTransform = {
  	Orange_: null,
  	Orange1_: null,
  	Orange4_: null,
  	Orange5_: null,
  	Orange6_: null,
  	Orange7_: null,
  	Orange8_: null,
  	Orange9_: null,
  	Orange10_: null,
  	Orange11_: null,
  	Orange12_: null,
  	Orange13_: null,
  	Orange14_: null,
  	Orange15_: null,
  	Orange16_: null,
  	Orange17_: null,
  	Orange18_: null,
  	winWidth_: 0,
  	winHeight_: 0,
  	orientation_: 0,
  	origBeta_: null,
  	origGamma_: null,
  	tx_: 0,
  	ty_: 0,
  	mx_: 0,
  	my_: 0,
  	transform_: null,

	init: function() {
    	this.Orange_ = document.getElementById('element2').style;
    	this.Orange1_ = document.getElementById('element3').style;
    	this.Orange3_ = document.getElementById('element3').style;
    	this.Orange4_ = document.getElementById('element4').style;
    	this.Orange5_ = document.getElementById('element5').style;
    	this.Orange6_ = document.getElementById('element6').style;
    	this.Orange7_ = document.getElementById('element7').style;
    	this.Orange8_ = document.getElementById('element8').style;
    	this.Orange9_ = document.getElementById('element9').style;
    	this.Orange10_ = document.getElementById('element10').style;
    	this.Orange11_ = document.getElementById('element11').style;
    	this.Orange12_ = document.getElementById('element12').style;
    	this.Orange13_ = document.getElementById('element13').style;
    	this.Orange14_ = document.getElementById('element14').style;
    	this.Orange15_ = document.getElementById('element15').style;
    	this.Orange16_ = document.getElementById('element16').style;
    	this.Orange17_ = document.getElementById('element17').style;
    	this.Orange18_ = document.getElementById('element18').style;
    	this.transform_ = this.getSupportedProp_(['transform', 'MozTransform', 'WebkitTransform', 'msTransform', 'OTransform']);

    	window.onresize = this.onResize_.bind(this);
    	this.onResize_();

    	this.render_();
    
    	window.ondeviceorientation = this.onDeviceOrientation_.bind(this);
    	window.onmousemove = this.onMouseMove_.bind(this);
  	},

  	getSupportedProp_: function(propArr) {
    	var root = document.documentElement;

    	for (var i = 0; i < propArr.length; i++) {
      		if (propArr[i] in root.style) {
        		return propArr[i];
      		}
    	}
  	},

  	onDeviceOrientation_: function(e) {
    	var beta = 0;
    	var gamma = 0;
	  
    	if (window.orientation === 180) {
      	beta = -e.beta;
      	gamma = -((e.alpha + e.gamma + 360) % 360);
    	} 
		else if (window.orientation === 90) {
		beta = -((e.gamma + 180) % 180);
      	gamma = ((e.beta + 270) % 180) * 2;
    	} 
		else if (window.orientation === -90) {
      	beta = (e.gamma + 180) % 180;
      	gamma = -((e.beta + 270) % 180) * 2;
    	} 
		else {
      	beta = e.beta;
      	gamma = (e.alpha + e.gamma + 360) % 360;
    	}

    	if (beta !== 0 && gamma !== 0) {
      		if (!this.origBeta_ || this.orientation_ !== window.orientation) {
        		this.origBeta_ = beta;
        		this.origGamma_ = gamma;
        		this.orientation_ = window.orientation;
      		}

      		this.tx_ = ((gamma - this.origGamma_) % 360) / 30;
      		this.ty_ = ((beta - this.origBeta_ + 180) % 360 - 180) / 30;
      		this.origBeta_ += Math.max(this.ty_ - 1, 0);
      		this.origBeta_ += Math.min(this.ty_ + 1, 0);
      		this.origGamma_ += Math.max(this.tx_ - 1, 0);
      		this.origGamma_ += Math.min(this.tx_ + 1, 0);
      		this.tx_ = Math.max(Math.min(this.tx_, 1), -1);
      		this.ty_ = Math.max(Math.min(this.ty_, 1), -1);
    	}
  	},

  	onMouseMove_: function(e) {
    	this.tx_ = e.pageX / this.winWidth_ * 2 - 1;
    	this.ty_ = e.pageY / this.winHeight_ * 2 - 1;
  	},

  	onResize_: function() {
    	this.winWidth_ = window.innerWidth;
    	this.winHeight_ = window.innerHeight;
  	},

  	render_: function() {
    	requestAnimationFrame(this.render_.bind(this));
    
    	this.mx_ += (this.tx_ - this.mx_) * 0.25;
    	this.my_ += (this.ty_ - this.my_) * 0.25;
	  
    	this.Orange_[this.transform_] = 'perspective(70vw) scale3d(2,2,1) rotateY(' + (this.mx_ * 1) + 'deg) rotateX(' + (-this.my_ * 1) + 'deg) translateZ(-70vw)';
    	this.Orange1_[this.transform_] = 'perspective(65vw) scale3d(2,2,1) rotateY(' + (this.mx_ * 1) + 'deg) rotateX(' + (-this.my_ * 1) + 'deg) translateZ(-65vw)';
    	this.Orange3_[this.transform_] = 'perspective(60vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 0.5) + 'deg) rotateX(' + (this.my_ * 0.5) + 'deg) translateZ(-60vw)';
    	this.Orange4_[this.transform_] = 'perspective(50vw) scale3d(2,2,1) rotateY(' + ( this.mx_ * 1.5) + 'deg) rotateX(' + (- this.my_ * 1.5) + 'deg) translateZ(-50vw)';
    	this.Orange5_[this.transform_] = 'perspective(45vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-45vw)';
    	this.Orange6_[this.transform_] = 'perspective(40vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-40vw)';
    	this.Orange7_[this.transform_] = 'perspective(30vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-30vw)';
    	this.Orange8_[this.transform_] = 'perspective(20vw) scale3d(2,2,1) rotateY(' + ( this.mx_ * 1) + 'deg) rotateX(' + (-this.my_ * 1) + 'deg) translateZ(-20vw)';
    	this.Orange9_[this.transform_] = 'perspective(10vw) scale3d(2,2,1) rotateY(' + ( this.mx_ * 1.5) + 'deg) rotateX(' + (- this.my_ * 1.5) + 'deg) translateZ(-10vw)';
    	this.Orange10_[this.transform_] = 'perspective(5vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-5vw)';
    	this.Orange11_[this.transform_] = 'perspective(30vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-30vw)';
    	this.Orange12_[this.transform_] = 'perspective(90vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-90vw)';
    	this.Orange13_[this.transform_] = 'perspective(90vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-90vw)';
    	this.Orange14_[this.transform_] = 'perspective(50vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 1.5) + 'deg) rotateX(' + (this.my_ * 1.5) + 'deg) translateZ(-50vw)';
    	this.Orange15_[this.transform_] = 'perspective(10vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 4.5) + 'deg) rotateX(' + (this.my_ * 4.5) + 'deg) translateZ(-10vw)';
    	this.Orange16_[this.transform_] = 'perspective(30vw) scale3d(2,2,1) rotateY(' + ( this.mx_ * 9.5) + 'deg) rotateX(' + ( - this.my_ * 9.5) + 'deg) translateZ(-30vw)';
    	this.Orange17_[this.transform_] = 'perspective(10vw) scale3d(2,2,1) rotateY(' + ( this.mx_ * 2.5) + 'deg) rotateX(' + (- this.my_ * 2.5) + 'deg) translateZ(-10vw)';
    	this.Orange18_[this.transform_] = 'perspective(90vw) scale3d(2,2,1) rotateY(' + (- this.mx_ * 3) + 'deg) rotateX(' + (this.my_ * 2) + 'deg) translateZ(-90vw)';
	},
};